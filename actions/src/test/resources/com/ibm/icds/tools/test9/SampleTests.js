describe("sample", function() {
    var sample = System.getModule("com.ibm.icds.tools.test9").sample;
    it("should add two numbers", function() {
        expect(sample(5, 2)).toBe(8);
    });
});
